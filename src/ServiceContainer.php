<?php

namespace Kubomikita;

use Nette\DI\Container;

class Service {
	protected static $services = array();
	protected static $containerKey = "context";

	/**
	 * @deprecated
	 */
	public static function set($key, $value) : void {
		static::add($key, $value);
	}

	public static function add($key,$value){
		if(isset(static::$services[$key])){
			throw new \Exception("Service with key '$key' already exists.",500);
		}
		static::$services[$key] = $value;
	}

	/**
	 * @param $key
	 *
	 * @return mixed|object
	 * @throws \Exception
	 */
	public static function get(string $key){
		if($key == "container"){
			$key = "configurator";
		}
		
		if(!isset(static::$services[$key])){
			if(isset(static::$services[static::$containerKey])) {
				$di = static::$services[ self::$containerKey ];
				if ( $di instanceof Container ) {
					return static::$services[$key] = $di->getService( $key );
				}
			}
			throw new \Exception("Service with key '$key' not exists.",500);
		}

		return static::$services[$key];
	}

	/**
	 *
	 * @return object|null
	 */
	public static function getByType(string $key) {
		if(!isset(static::$services[self::$containerKey])){
			return static::get('configurator')->getByType($key);
		}
		return static::$services[self::$containerKey]->getByType($key);
	}

	public static function debug(){
		return self::$services;
	}
}
